## 代码高亮样式

主题默认样式为`highlight-one-light.css`，当前提供：`highlight-dark.css`、`highlight-one-light.css`两款代码样式；主题样式基于仓库[PrismJS Themes](https://github.com/PrismJS/prism-themes)样式微调而来，具体支持语言样式请参考[Prism.js](https://prismjs.com)。

使用主题时，请将`code_highlight`文件夹下样式，复制重命名替换主题根目录下`highlight.css`样式，重启Vnotex即可生效。

```ini
highlight-dark.css #dark样式
highlight-one-light.css #light样式
YaHei Consolas Hybrid 1.12.ttf #主题首选字体，可选
```

