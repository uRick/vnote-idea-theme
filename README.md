<div style="font-size:50px"><span style="color: #ff0000">v</span><span style="color: #ff4000">n</span><span style="color: #ff7f00">o</span><span style="color: #00d2ff">t</span><span style="color: #00e980">e</span><span style="color: #00ff00"> </span><span style="color: #00ff80">i</span><span style="color: #00ffff">d</span><span style="color: #5f9090">e</span><span style="color: #7548c8">a</span><span style="color: #8b00ff"> </span><span style="color: #c50080">t</span><span style="color: #ff0000">h</span><span style="color: #ff7f00">e</span><span style="color: #de4d67">m</span><span style="color: #bc1bce">e</span></div>



自定义方式参考[Vnote](https://tamlok.github.io/vnote/zh_cn/)官方文档：[主题和样式](https://tamlok.github.io/vnote/zh_cn/#!docs/%E7%94%A8%E6%88%B7/%E4%B8%BB%E9%A2%98%E5%92%8C%E6%A0%B7%E5%BC%8F.md)

`vx-idea`主题又名`石板灰`，适用于新版本[vnotex](https://vnotex.github.io/vnote/zh_cn/#!vnotex.md)版本；vnote 2.0` 版本请切换至` vnote 分支。



## 1. 配置

### 1.1 配置步骤

- 打开路径：`Cltr+Alt+P` --> `主题` --> `添加/删除`;

- 将主题文件 `vx-idea` 文件全部复制至打开路径下，刷新并应用即可;

- 代码高亮请查看 `code_higlight` 文件夹内容。

### 1.2 alert组件增强[可选]

 基于vnotex原生 `alert` 组件支持，引入图标增强，主要为编写技术文档适配；当前不支持 `alert-light`、`alert-success`。
 效果如下：

![提示组件预览效果](img/image-202108118143937472.png)

- 复制 `tips_components/style.css` 中样式——>追加至 `web.css` 文件尾即可。

## 2. 预览


![主题效果图](img/image-202108118143937471.png)

## 3. 鸣谢

- [tamlok](https://github.com/tamlok) - [VNote](https://github.com/tamlok/vnote)
